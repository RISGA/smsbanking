package sgo.mobile.com.smsbanking.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import sgo.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class PurchaseActivity extends AppCompatActivity {

    private ListActivity listActivity;
    public String purchaseCategory;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        listActivity = new ListActivity();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.purchase_layout);
        // storing string resources into Array
        String[] listData = getResources().getStringArray(R.array.purchase_list);

        // Binding resources Array to ListAdapter


        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this, R.layout.list_item_arrow, R.id.label, listData);
        ListView lv = (ListView)findViewById(R.id.lv);
        lv.setAdapter(adapter);



        // listening to single list item on click
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

//                AlertDialog.Builder builder = new AlertDialog.Builder(PurchaseActivity.this);
//                builder.setMessage("We Apologize, it will be available soon")
//                        .setTitle("Purchase")
//                        .setPositiveButton(android.R.string.ok, null);
//                AlertDialog dialog = builder.create();
//                dialog.show();
                purchaseCategory=(String)parent.getItemAtPosition(position);
                Intent intent = new Intent(PurchaseActivity.this, PurchaseGeneralActivity.class);
                intent.putExtra("purchaseCategory", purchaseCategory);
                startActivity(intent);




            }
        });
    }
}
