package sgo.mobile.com.smsbanking.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.securepreferences.SecurePreferences;

import sgo.mobile.com.smsbanking.AppHelper;
import sgo.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 11/15/2016.
 */
public class ChangeProtocolActivity extends AppCompatActivity {

    RadioGroup radioGroup;
    RadioButton radioSMS, radioUSSD;
    Button btnSave, btnBack;
    String defautProtocol;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.change_protocol_layout);

        radioGroup = (RadioGroup)findViewById(R.id.radioGroupProtocol);
        radioSMS = (RadioButton)findViewById(R.id.radioSMS);
        radioUSSD = (RadioButton)findViewById(R.id.radioUSSD);
        btnSave = (Button)findViewById(R.id.btnSave);

        defautProtocol = AppHelper.getProtocol(getApplicationContext());
        if(defautProtocol.equalsIgnoreCase("sms")){
            radioSMS.setChecked(true);

        }else if(defautProtocol.equalsIgnoreCase("ussd")){
            radioUSSD.setChecked(true);
        }



        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                if (radioSMS.isChecked()) {

                    defautProtocol = "SMS";
                } else if (radioUSSD.isChecked()) {
                    defautProtocol = "USSD";

                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {


                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        ChangeProtocolActivity.this);

                // set title
                alertDialogBuilder.setTitle("Jalur Komunikasi");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Apakah anda yakin merubah jalur komunikasi ?")
                        .setCancelable(false)
                        .setPositiveButton("YA",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {


                                SecurePreferences prefs = new SecurePreferences(getApplicationContext());
                                SecurePreferences.Editor mEditor = prefs.edit();
                                mEditor.putString("user_protocol", defautProtocol);
                                mEditor.apply();
                                mEditor.commit();

                                Toast.makeText(getApplicationContext(), "JALUR KOMUNIKASI TELAH DIRUBAH : " + defautProtocol,
                                        Toast.LENGTH_LONG).show();

                            }
                        })
                        .setNegativeButton("TIDAK",null);

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();
            }
        });




    }
}
