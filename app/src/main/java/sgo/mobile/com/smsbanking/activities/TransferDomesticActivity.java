package sgo.mobile.com.smsbanking.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import sgo.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class TransferDomesticActivity extends AppCompatActivity {
    AutoCompleteTextView sourceAccount, benefAccount, AutoBank;
    EditText nominal;
    Button btnSubmit;

    String[] sourceAcct={"901999123881","90288123771"};
    String[] BenefAcct={"90001230012","99912881231"};
    String[] bankList={"Bank Indonesia",
            "Bank Negara Indonesia",
            "Bank Rakyat Indonesia",
            "Bank Tabungan Negara",
            "Bank Mandiri",
            "Bank Mutiara",
            "Bank Agroniaga",
            "Bank Anda",
            "Bank Artha Graha Internasional",
            "Bank Bukopin",
            "Bank Bumi Arta",
            "Bank Capital Indonesia",
            "Bank Central Asia",
            "Bank CIMB Niaga",
            "Bank Danamon",
            "Bank Ekonomi Raharja",
            "Bank Ganesha",
            "Bank Hana",
            "Bank Himpunan Saudara 1906",
            "Bank ICB Bumiputera",
            "Bank ICBC Indonesia",
            "Bank Index Selindo",
            "Bank Internasional Indonesia Maybank",
            "Bank Kesawan",
            "Bank Maspion",
            "Bank Mayapada",
            "Bank Mega",
            "Bank Mestika Dharma",
            "Bank Metro Express",
           "Bank Muamalat Indonesia",
            "Bank Nusantara Parahyangan",
            "Bank OCBC NISP",
            "Bank Permata",
            "Bank SBI Indonesia",
            "Bank Sinarmas",
            "Bank Swadesi",
            "Bank Syariah Mandiri",
            "Bank Syariah Mega Indonesia",
            "Bank Victoria Internasional",
            "Bank Pan Indonesia Bank",
            "Bank Anglomas Internasional Bank",
            "Bank Andara",
            "Bank Artos Indonesia",
            "Bank Barclays Indonesia",
            "Bank Bisnis Internasional",
            "Bank BRI Syariah",
            "Bank Central Asia Syariah",
            "Bank Dipo International",
            "Bank Fama Internasional",
            "Bank Harda Internasional",
            "Bank Ina Perdana",
            "Bank Jasa Jakarta",
            "Bank Kesejahteraan Ekonomi",
            "Bank Liman International",
            "Bank Mayora",
            "Bank ANZ Panin Bank",
            "Bank Agris",
            "Bank Commonwealth",
            "Bank BNP Paribas Indonesia",
            "Bank DBS Indonesia",
            "Bank KEB Indonesia",
            "Bank Maybank Syariah Indonesia",
            "Bank Mizuho Indonesia",
            "Bank UOB Buana",
            "Bank Rabobank Internasional Indonesia",
            "Bank Resona Perdania",
            "Bank Windu Kentjana Internasional",
            "Bank Woori Indonesia",
            "Bank Chinatrust Indonesia",
            "Bank Sumitomo Mitsui Indonesia",
           "Bank UFJ Indonesia",
            "Bank Mitraniaga",
            "Bank Multi Arta Sentosa",
            "Bank Nationalnobu",
            "Bank Pundi Indonesia",
            "Bank Purba Danarta",
            "Bank Royal Indonesia",
            "Bank Sinar Harapan Bali",
            "Bank STMIK Binamulia",
            "Bank Syariah Bukopin",
            "Bank Tabungan Pensiunan Nasional",
            "Bank Victoria Syariah",
           "Bank Yudha Bhakti",
            "Bank Centratama Nasional Bank",
            "Bank Pan Indonesia Bank Syariah",
            "Bank Prima Master Bank",
            "Bank Jambi",
            "Bank Kalsel",
            "Bank Kaltim",
            "Bank Sultra",
            "Bank BPD DIY",
            "Bank Nagari",
            "Bank DKI",
            "Bank Lampung",
            "Bank Kalteng",
            "Bank BPD Aceh",
            "Bank Sulsel",
            "Bank BJB",
            "Bank Kalbar",
            "Bank Maluku",
            "Bank Bengkulu",
            "Bank Jateng",
            "Bank Jatim",
            "Bank NTB",
            "Bank NTT",
            "Bank Sulteng",
            "Bank Sulut",
            "Bank BPD Bali",
            "Bank Papua",
            "Bank Riau Kepri",
            "Bank Sumsel Babel",
            "Bank Sumut",
            "Bank BNI Syariah",
            "Bank BRI Syariah",
            "Bank Maybank Syariah Indonesia",
            "Bank Mega Syariah Indonesia",
            "Bank Muamalat Indonesia",
            "Bank Syariah Bukopin",
            "Bank Syariah Mandiri",
           "Bank Victoria Syariah",
            "Bank Pan Indonesia Bank Syariah",
            "Bank CIMB Niaga Syariah",
            "Bank OCBC NISP Syariah",
           "Bank Danamon Syariah",
            "Bank Riau Kepri Syariah",
            "Bank BCA Syariah",
            "Bank BJB Syariah",
            "Bank Permata Syariah"};
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.domestic_transfer_layout);

        sourceAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
        benefAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        AutoBank=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView3);
        nominal =(EditText)findViewById(R.id.txt_nominal);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
        sourceAccount.setAdapter(adapter);
        sourceAccount.setThreshold(1);

        ArrayAdapter adapters = new ArrayAdapter(this,android.R.layout.simple_list_item_1,BenefAcct);
        benefAccount.setAdapter(adapters);
        benefAccount.setThreshold(1);

        ArrayAdapter adapte = new ArrayAdapter(this,android.R.layout.simple_list_item_1,bankList);
        AutoBank.setAdapter(adapte);
        AutoBank.setThreshold(1);
        final InputMethodManager imm = (InputMethodManager)getApplication().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nominal.getWindowToken(), 0);

        nominal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (sourceAccount.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferDomesticActivity.this);
                        builder.setMessage("Please, select source account")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }if(AutoBank.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferDomesticActivity.this);
                        builder.setMessage("Please, select bank")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;

                    }
                    if (benefAccount.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferDomesticActivity.this);
                        builder.setMessage("Please, select beneficiary")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }if (nominal.getText().toString().equals("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferDomesticActivity.this);
                        builder.setMessage("Please, input nominal")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }else {
                        launchRingDialog();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }

        });
    }

    public void launchRingDialog() {

        final ProgressDialog ringProgressDialog = ProgressDialog.show(TransferDomesticActivity.this, "Please wait ...", "Processing ...", true);

        ringProgressDialog.setCancelable(true);

        new Thread(new Runnable() {

            public void run() {

                try {

                    Thread.sleep(10000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TransferDomesticActivity.this);
                            builder.setMessage("Transfer Success")
                                    .setTitle("Transfer")
                                    .setPositiveButton(android.R.string.ok, null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                } catch (Exception e) {

                }

                ringProgressDialog.dismiss();



            }

        }).start();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id != null) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
}
