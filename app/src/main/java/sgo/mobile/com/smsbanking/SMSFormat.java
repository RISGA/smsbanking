package sgo.mobile.com.smsbanking;

import android.app.ProgressDialog;
import android.content.Context;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by MOBILEDEV on 10/31/2016.
 */
public class SMSFormat {
    ProgressDialog progressDialog;
    Context context;
    public SMSFormat(Context _context){
        this.context=_context;
    }

    public void sendSMSMessage(String nomor,String konten) {

//        progressDialog= new ProgressDialog(context);
//        progressDialog.show();
//        progressDialog = ProgressDialog.show(context, "Mohon menunggu ...",
//                "Permintaan anda sedang diprosess ...", true);
        String phoneNo = nomor;
        String sms = konten;

        try {
            android.telephony.gsm.SmsManager smsManager = android.telephony.gsm.SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, sms, null, null);
            Toast.makeText(context, "SMS Sent!" + konten,
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(context,
                    "SMS faild, please try again later!",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

//        progressDialog.dismiss();
    }


}
