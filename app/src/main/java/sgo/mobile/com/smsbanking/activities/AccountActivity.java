package sgo.mobile.com.smsbanking.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sgo.mobile.com.smsbanking.AppHelper;
import sgo.mobile.com.smsbanking.R;
import sgo.mobile.com.smsbanking.SMSFormat;

/**
 * Created by MOBILEDEV on 10/26/2016.
 */
public class AccountActivity extends AppCompatActivity {
    Button btnAccInfo, btnAccStatement;
    public String defaultProtocol,ussdCode;
    private ProgressDialog pDialog;
    SMSFormat smsFormat;
    ProgressDialog progressDialog;
    MainActivity mainActivity;
    public String NO_HP, KONTEN_SMS, PIN, KODE_REK, USER_PIN;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_account);

        btnAccInfo = (Button) findViewById(R.id.menu_acc_info);
        btnAccStatement = (Button) findViewById(R.id.menu_acc_sttment);
        Animation anim_left = AnimationUtils.loadAnimation(getApplication(), R.anim.push_left);
        Animation anim_right = AnimationUtils.loadAnimation(getApplication(), R.anim.push_right);
        btnAccInfo.startAnimation(anim_left);
        btnAccStatement.startAnimation(anim_right);
        KODE_REK = getResources().getString(R.string.KODE_REK);
        NO_HP = getResources().getString(R.string.NO_HP);
        USER_PIN = AppHelper.getPIN(getApplication());


        btnAccInfo.setOnClickListener(myButtonListener);
        btnAccStatement.setOnClickListener(myButtonListener);

        defaultProtocol = AppHelper.getProtocol(getApplicationContext());


    }

    private View.OnClickListener myButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.menu_acc_info:

                    if (defaultProtocol.equalsIgnoreCase("SMS")) {
                        KONTEN_SMS = getResources().getString(R.string.SMS_SALDO);
                        showPinUnlockScreen();
                    } else {
                        ussdCode = "*" + "141" + "*" + "1" + "*" + "1" +  Uri.encode("#");
                        if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));
                    }



                    break;
                case R.id.menu_acc_sttment:
                    if(defaultProtocol.equalsIgnoreCase("SMS")){
                        KONTEN_SMS = getResources().getString(R.string.SMS_MUTASI_REKENING);
                        showPinUnlockScreen();
                    }else{
                        ussdCode = "*" + "141" + "*" + "1" + "*" + "2" + Uri.encode("#");
                        if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        if (ActivityCompat.checkSelfPermission(AccountActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + ussdCode)));

                    }

                    break;
            }
        }
    };

    private void showPinUnlockScreen() {
//        Intent intent = new Intent(this, PinUnlockActivity.class);
//        intent.putExtra("NO_HP", NO_HP);
//        intent.putExtra("PIN", USER_PIN);
//        intent.putExtra("KODE_REK", KODE_REK);
//        intent.putExtra("KONTEN_SMS", KONTEN_SMS);
//        startActivity(intent);


            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.pin_layout, null);
            dialogBuilder.setView(dialogView);

            final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

            dialogBuilder.setTitle("PIN");
            dialogBuilder.setMessage("Masukan PIN sms banking anda");

            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {

                    PIN = edt.getText().toString();
                    String KONTEN = KONTEN_SMS+" "+PIN;

//                    smsFormat = new SMSFormat(getApplication());
//                    smsFormat.sendSMSMessage(NO_HP,KONTEN);
//                    Toast.makeText(getApplication(), KONTEN,
//                            Toast.LENGTH_LONG).show();


                    try {
                        pDialog = ProgressDialog.show(AccountActivity
                                .this, "SMS Banking",
                                "sedang memproses ...", true);
                        String SENT = "sent";
                        String DELIVERED = "delivered";

                        Intent sentIntent = new Intent(SENT);
     /*Create Pending Intents*/
                        PendingIntent sentPI = PendingIntent.getBroadcast(
                                getApplicationContext(), 0, sentIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT);

                        Intent deliveryIntent = new Intent(DELIVERED);

                        PendingIntent deliverPI = PendingIntent.getBroadcast(
                                getApplicationContext(), 0, deliveryIntent,
                                PendingIntent.FLAG_UPDATE_CURRENT);
     /* Register for SMS send action */
                        registerReceiver(new BroadcastReceiver() {

                            @Override
                            public void onReceive(Context context, Intent intent) {
                                String result = "";

                                switch (getResultCode()) {

                                    case Activity.RESULT_OK:
                                        result = "Transmission successful";
                                        break;
                                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                                        result = "Transmission failed";
                                        break;
                                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                                        result = "Radio off";
                                        break;
                                    case SmsManager.RESULT_ERROR_NULL_PDU:
                                        result = "No PDU defined";
                                        break;
                                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                                        result = "No service";
                                        break;
                                }

                                Toast.makeText(getApplicationContext(), result,
                                        Toast.LENGTH_LONG).show();
                                pDialog.dismiss();
                            }

                        }, new IntentFilter(SENT));
     /* Register for Delivery event */
                        registerReceiver(new BroadcastReceiver() {

                            @Override
                            public void onReceive(Context context, Intent intent) {
                                Toast.makeText(getApplicationContext(), "Deliverd",
                                        Toast.LENGTH_LONG).show();
                            }

                        }, new IntentFilter(DELIVERED));

      /*Send SMS*/
                        SmsManager smsManager = SmsManager.getDefault();
                        smsManager.sendTextMessage(NO_HP, null, KONTEN, sentPI,
                                deliverPI);
                    } catch (Exception ex) {
                        Toast.makeText(getApplicationContext(),
                                ex.getMessage().toString(), Toast.LENGTH_LONG)
                                .show();
                        ex.printStackTrace();




                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                                getApplication());

                        // set title
                        alertDialogBuilder.setTitle("SMS Banking");

                        // set dialog message
                        alertDialogBuilder
                                .setMessage("Silahkan cek pesan masuk anda")
                                .setCancelable(false)
                                .setPositiveButton("Yes",null);

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();

                        // show it
                        alertDialog.show();
                    }








                }
            });
            dialogBuilder.setNegativeButton("Batal",null);
            AlertDialog b = dialogBuilder.create();

            b.show();




    }

    private void showPinSetupScreen() {
        Intent intent = new Intent(this, PinSetupActivity.class);
        startActivity(intent);



    }

    public void launchRingDialog() {

        progressDialog = ProgressDialog.show(AccountActivity.this, "Mohon menunggu ...", "Sedang memproses ...", true);

        progressDialog.setCancelable(true);

        new Thread(new Runnable() {

            public void run() {

                try {

                    Thread.sleep(1000);


                } catch (Exception e) {

                }

                progressDialog.dismiss();



            }

        }).start();

    }




//    public void checkSaldo(){
//
//
//
//        progressDialog = ProgressDialog.show(AccountActivity.this, "Mohon menunggu ...",
//                "Permintaan anda sedang diprosess ...", true);
//        String phoneNo = noHP;
//        String sms = kontenSms;
//
//        try {
//            SmsManager smsManager = SmsManager.getDefault();
//            smsManager.sendTextMessage(phoneNo, null, sms, null, null);
//            Toast.makeText(getApplicationContext(), "SMS Sent!",
//                    Toast.LENGTH_LONG).show();
//        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(),
//                    "SMS faild, please try again later!",
//                    Toast.LENGTH_LONG).show();
//            e.printStackTrace();
//        }
//
//        progressDialog.dismiss();
//    }


}
