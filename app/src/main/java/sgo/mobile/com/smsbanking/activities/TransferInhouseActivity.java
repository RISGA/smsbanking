package sgo.mobile.com.smsbanking.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import sgo.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class TransferInhouseActivity extends AppCompatActivity {

    AutoCompleteTextView sourceAccount, benefAccount;
    EditText nominal;
    Button btnSubmit;

    String[] sourceAcct={"901999123881","90288123771"};
    String[] BenefAcct={"90001230012","99912881231"};
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inhouse_transfer_layout);

        sourceAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
        benefAccount=(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView2);
        nominal =(EditText)findViewById(R.id.txt_nominal);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);

        ArrayAdapter adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,sourceAcct);
        sourceAccount.setAdapter(adapter);
        sourceAccount.setThreshold(1);

        ArrayAdapter adapters = new ArrayAdapter(this,android.R.layout.simple_list_item_1,BenefAcct);
        benefAccount.setAdapter(adapters);
        benefAccount.setThreshold(1);
        final InputMethodManager imm = (InputMethodManager)getApplication().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(nominal.getWindowToken(), 0);

        nominal.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                imm.showSoftInputFromInputMethod(nominal.getWindowToken(), 0);
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (sourceAccount.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivity.this);
                        builder.setMessage("Please, select source account")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }
                    if (benefAccount.getText().toString().equals("")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivity.this);
                        builder.setMessage("Please, select beneficiary")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }if (nominal.getText().toString().equals("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivity.this);
                        builder.setMessage("Please, input nominal")
                                .setTitle("Transfer")
                                .setPositiveButton(android.R.string.ok, null);
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return;
                    }else {
                        launchRingDialog();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }



            }

        });
    }

    public void launchRingDialog() {

        final ProgressDialog ringProgressDialog = ProgressDialog.show(TransferInhouseActivity.this, "Please wait ...", "Processing ...", true);

        ringProgressDialog.setCancelable(true);

        new Thread(new Runnable() {

            public void run() {

                try {

                    Thread.sleep(10000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            AlertDialog.Builder builder = new AlertDialog.Builder(TransferInhouseActivity.this);
                            builder.setMessage("Transfer Success")
                                    .setTitle("Transfer")
                                    .setPositiveButton(android.R.string.ok, null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    });

                } catch (Exception e) {

                }

                ringProgressDialog.dismiss();



            }

        }).start();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id != null) {
//            return true;
//        }
        return super.onOptionsItemSelected(item);
    }
}
