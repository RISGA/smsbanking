package sgo.mobile.com.smsbanking.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import sgo.mobile.com.smsbanking.AppHelper;
import sgo.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/27/2016.
 */
public class TransferActivity extends AppCompatActivity {
    Button btnInhouse, btnDomestic;
    public String NO_HP, KONTEN_SMS,PIN, KODE_REK,USER_PIN;
    MainActivity mainActivity;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.transfer_layout);

        btnInhouse = (Button)findViewById(R.id.menu_transfer_inhouse);
        btnDomestic = (Button)findViewById(R.id.menu_transfer_domestic);

        Animation anim_left = AnimationUtils.loadAnimation(getApplication(), R.anim.push_left);
        Animation anim_right = AnimationUtils.loadAnimation(getApplication(), R.anim.push_right);
        btnInhouse.startAnimation(anim_left);
        btnDomestic.startAnimation(anim_right);
        KODE_REK = getResources().getString(R.string.KODE_REK);
        NO_HP = getResources().getString(R.string.NO_HP);
        USER_PIN = AppHelper.getPIN(getApplication());
        btnDomestic.setOnClickListener(myButtonListener);
        btnInhouse.setOnClickListener(myButtonListener);







    }

    private View.OnClickListener myButtonListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.menu_transfer_inhouse:
                    try {

                        if (USER_PIN.equals("")) {
                            mainActivity = new MainActivity();
                            mainActivity.showPINSetupScreen();
                        } else {
                            KONTEN_SMS = getResources().getString(R.string.SMS_TRANSFER_INHOUSE);
                            showPinUnlockScreen();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    break;
                case R.id.menu_transfer_domestic:

                    try {

                        if (USER_PIN.equals("")) {
                            mainActivity = new MainActivity();
                            mainActivity.showPINSetupScreen();
                        } else {
                            KONTEN_SMS = getResources().getString(R.string.SMS_TRANSFER_DOMESTIC);
                            showPinUnlockScreen();

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };


    private void showPinUnlockScreen() {
        Intent intent = new Intent(this, PinUnlockActivity.class);
        intent.putExtra("NO_HP", NO_HP);
        intent.putExtra("PIN", USER_PIN);
        intent.putExtra("KODE_REK", KODE_REK);
        intent.putExtra("KONTEN_SMS", KONTEN_SMS);
        startActivity(intent);



    }
}
