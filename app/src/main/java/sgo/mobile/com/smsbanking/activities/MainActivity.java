package sgo.mobile.com.smsbanking.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Toast;

import com.securepreferences.SecurePreferences;
import com.victor.loading.rotate.RotateLoading;

import cn.pedant.SweetAlert.SweetAlertDialog;
import sgo.mobile.com.smsbanking.AppHelper;
import sgo.mobile.com.smsbanking.R;

public class MainActivity extends AppCompatActivity {

    Button btnAccInfo,btnTransfer,btnPurchase,btnPayment,btnSetting,btnInfo;
    RotateLoading rotateLoading;
    Context context;
    MainActivity mainActivity;
    public String noHP, kontenSms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//showPINSetupScreen();
//


        rotateLoading = (RotateLoading)findViewById(R.id.rotateloading);
        btnAccInfo = (Button)findViewById(R.id.menu_acc_info);
        btnPurchase= (Button)findViewById(R.id.menu_purchase);
        btnPayment= (Button)findViewById(R.id.menu_payment);
        btnTransfer= (Button)findViewById(R.id.menu_transfer);
        btnSetting= (Button)findViewById(R.id.menu_setting);
        btnInfo= (Button)findViewById(R.id.menu_info);
        btnInfo.setClickable(false);
        btnInfo.setVisibility(View.GONE);
        Animation anim_left = AnimationUtils.loadAnimation(getApplication(), R.anim.push_left);
        Animation anim_right = AnimationUtils.loadAnimation(getApplication(), R.anim.push_right);
        btnAccInfo.startAnimation(anim_left);
        btnTransfer.startAnimation(anim_right);
        btnPurchase.startAnimation(anim_right);
        btnPayment.startAnimation(anim_left);
        btnSetting.startAnimation(anim_left);
//        btnInfo.startAnimation(anim_right);

        String userProtocol = AppHelper.getProtocol(getApplicationContext());

        Toast.makeText(getApplicationContext(),userProtocol,
                Toast.LENGTH_LONG).show();

        SecurePreferences prefs = new SecurePreferences(getApplicationContext());
        SecurePreferences.Editor mEditor = prefs.edit();
        mEditor.putString("user_protocol", userProtocol);
        mEditor.apply();
        mEditor.commit();


        btnAccInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                startActivity(intent);


            }

        });


        btnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, TransferActivity.class);
                startActivity(intent);


            }

        });

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, PurchaseActivity.class);
                startActivity(intent);


            }

        });

        btnPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, PaymentActivity.class);
                startActivity(intent);


            }

        });

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);


            }

        });



    }


    public void showPINSetupScreen() {
        Intent intent = new Intent(MainActivity.this, PinSetupActivity.class);
        startActivity(intent);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        SecurePreferences prefs = new SecurePreferences(getApplicationContext());

        prefs.edit().remove("user_pin").commit();


        // Stop method tracing that the activity started during onCreate()
        android.os.Debug.stopMethodTracing();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
              NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            new SweetAlertDialog(MainActivity.this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Are you sure want to quit?")
                    .setCancelText("No")
                    .setConfirmText("Yes")
                    .showCancelButton(true)
                    .setCancelClickListener(null)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            SecurePreferences prefs = new SecurePreferences(getApplicationContext());

                            prefs.edit().remove("user_pin").commit();

                            System.exit(0);
                            finish();
                        }
                    })
                    .show();


        }
        return super.onKeyDown(keyCode, event);
    }
}


