package sgo.mobile.com.smsbanking.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.kbeanie.pinscreenlibrary.views.PinEntrySetupListener;
import com.kbeanie.pinscreenlibrary.views.PinView;
import com.securepreferences.SecurePreferences;

import sgo.mobile.com.smsbanking.R;

/**
 * Created by MOBILEDEV on 10/26/2016.
 */
public class PinSetupActivity extends ActionBarActivity implements PinEntrySetupListener {

    private PinView pinView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_setup);
        pinView = (PinView) findViewById(R.id.pinView);
        pinView.setModeSetup(this);
    }

    @Override
    public void onPinEntered(String pin) {

    }

    @Override
    public void onPinConfirmed(String pin) {

    }

    @Override
    public void onPinMismatch() {

    }

    @Override
    public void onPinSet(String pin) {
        SecurePreferences prefs = new SecurePreferences(getApplicationContext());
        SecurePreferences.Editor mEditor = prefs.edit();
        mEditor.putString("user_pin", pin);
        mEditor.apply();
        mEditor.commit();

        finish();
    }
}